class Result
  # http://guides.rubyonrails.org/active_model_basics.html

  attr_accessor :full_name, :html_url, :description
  # attr_accessor :language, :license

  def initialize(attributes = {})
    @full_name = attributes[:full_name]
    @html_url = attributes[:html_url]
    @description = attributes[:description]
    # @language = attributes[:language]
    # @license = attributes[:license]
  end
end
