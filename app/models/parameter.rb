class Parameter
  include ActiveModel::Model

  attr_accessor :keyword

  def initialize(attributes = nil)
    parameters = attributes

    @gh = GithubResult.new(parameters)
    @gl = GitlabResult.new(parameters)
  end

  # def results
  #   gh = GithubResult.new(keyword)
  #   res = gh.get_results
  #   gl = GitlabResult.new(keyword)
  #   res += gl.get_results
  # end

  def hub_status
    @gh.get_status
  end

  def hub_results
    @gh.get_results
  end

  def lab_status
    @gl.get_status
  end

  def lab_results
    @gl.get_results
  end
end
