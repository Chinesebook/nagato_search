class GitlabResult
  include HTTParty

  attr_accessor :keyword

  base_uri 'gitlab.com'

  def initialize(attributes)
    @keyword = attributes[:keyword]

    if @keyword
      options = { query: { scope: 'projects', search: @keyword,
                           per_page: 5,
                           private_token: Rails.application.credentials.gitlab_private_token } }
      @response = self.class.get('/api/v4/search', options)
    end
  end

  def get_status
    @response.code == 200
  end

  def get_results
    response_hash = JSON.parse(@response.body)
    results = response_hash.map do |rh|
      Result.new(full_name:   rh['name_with_namespace'],
                 html_url:    rh['web_url'],
                 description: rh['description'])
    end
  end
end
