class GithubResult
  include HTTParty

  attr_accessor :keyword

  base_uri 'api.github.com'

  def initialize(attributes)
    @keyword = attributes[:keyword]

    if @keyword
      options = { query: { q: @keyword, per_page: 5 } }
      @response = self.class.get('/search/repositories', options)
    end
  end

  def get_status
    @response.code == 200
  end

  def get_results
    # results = response_hash['items'].inject([]) do |res, rh|
    #   res << Result.new(full_name:   rh['full_name'],
    #                     html_url:    rh['html_url'],
    #                     description: rh['description'])
    # end

    response_hash = JSON.parse(@response.body)
    results = response_hash['items'].map do |rh|
      Result.new(full_name:   rh['full_name'],
                 html_url:    rh['html_url'],
                 description: rh['description'])
    end
  end
end
