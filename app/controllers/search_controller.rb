class SearchController < ApplicationController
  def index
    @parameter = Parameter.new
  end

  def result
    @parameter = Parameter.new(parameter_params)

    @hub_status = @parameter.hub_status
    @lab_status = @parameter.lab_status
    @results = @parameter.hub_results + @parameter.lab_results
  end

  private
    def parameter_params
      params.require(:parameter).permit(:keyword)
    end
end
