Rails.application.routes.draw do
  resources :search do
    get 'result', on: :collection
  end

  root to: 'search#index'
end
