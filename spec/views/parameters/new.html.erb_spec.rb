require 'rails_helper'

RSpec.describe "parameters/new", type: :view do
  before(:each) do
    assign(:parameter, Parameter.new())
  end

  it "renders new parameter form" do
    render

    assert_select "form[action=?][method=?]", parameters_path, "post" do
    end
  end
end
