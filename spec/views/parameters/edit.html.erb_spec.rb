require 'rails_helper'

RSpec.describe "parameters/edit", type: :view do
  before(:each) do
    @parameter = assign(:parameter, Parameter.create!())
  end

  it "renders the edit parameter form" do
    render

    assert_select "form[action=?][method=?]", parameter_path(@parameter), "post" do
    end
  end
end
