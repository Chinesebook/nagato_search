require 'rails_helper'

RSpec.describe Parameter, type: :model do
  context 'when get searching for "rails"' do
    # let(:parameter_params) { double(:params, parameter: { keyword: 'rails' }) }
    let(:par) do
      VCR.use_cassette 'par_hub&lab' do
        Parameter.new(keyword: 'rails')
      end
    end

    it 'all http code should be success' do
      expect(par.hub_status).to be_truthy
      expect(par.lab_status).to be_truthy
    end

    it 'should have total 10 results' do
      total = par.hub_results.count + par.lab_results.count
      expect(total).to eq(10)
    end

    it 'should have at least one result name contain "rails"' do
      res =  par.hub_results + par.lab_results
      expect(res.find { |res| res.full_name.include? 'rails' }).to_not be_nil
    end

    it 'every result should be unique' do
      res =  par.hub_results + par.lab_results
      expect(res.uniq.count).to eq(10)
    end
  end
end
