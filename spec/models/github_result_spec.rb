require 'rails_helper'

RSpec.describe GithubResult, type: :model do
  context 'when webmock work normally' do
    before do
      stub_request(:any, 'www.example.com').
        to_return(body: 'abc')
    end

    it 'should stubbing and setting correct response' do
      response = HTTParty.get('http://www.example.com')
      expect(response.body).to include('abc')
    end
  end

  context 'when get searching for "rails"' do
    let(:gh) do
      VCR.use_cassette 'github/rails' do
        GithubResult.new(keyword: 'rails')
      end
    end

    it 'should get 200 status' do
      expect(gh.get_status).to be_truthy
    end

    it 'should get five results' do
     expect(gh.get_results.count).to eq(5)
    end

    it 'should have at least one result name contain "rails"' do
      expect(gh.get_results.find { |res| res.full_name.include? 'rails' }).to_not be_nil
    end

    it 'every result should be unique' do
      expect(gh.get_results.uniq.count).to eq(5)
    end

  end
end
