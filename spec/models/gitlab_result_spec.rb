require 'rails_helper'

RSpec.describe GitlabResult, type: :model do
  context 'when get searching for "rails"' do
    let(:gl) do
      VCR.use_cassette 'gitlab/rails' do
        GitlabResult.new(keyword: 'rails')
      end
    end

    it 'should get 200 status' do
      expect(gl.get_status).to be_truthy
    end

    it 'should get five results' do
      expect(gl.get_results.count).to eq(5)
    end

    it 'should have at least one result name contain "rails"' do
      expect(gl.get_results.find { |res| res.full_name.include? 'rails' }).to_not be_nil
    end

    it 'every result should be unique' do
      expect(gl.get_results.uniq.count).to eq(5)
    end
  end
end
